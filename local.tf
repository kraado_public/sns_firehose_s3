locals {
  default_tags = {
    IACTool      = "terraform"
    Environment  = var.environment
    BillingGroup = var.billing_group
    Service      = var.service_name
    Purpose      = var.service_name
    Details      = "This service is used by ${var.service_name}"
  }

  tags = merge(local.default_tags, var.tags)
}


locals {
  // format bucket name correctly
  s3_name_fmt = replace(var.stream_name, "_", "-")
  s3_name     = var.firehose_bucket_arn == null ? "firehose-${local.s3_name_fmt}" : ""
  firehose_endpoint = var.firehose_endpoint_arn == null ? aws_kinesis_firehose_delivery_stream.s3_delivery[0].arn : var.firehose_endpoint_arn
}
