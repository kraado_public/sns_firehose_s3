#----------------------------------------
# CREATE KINESIS FIREHOSE STREAM
#----------------------------------------
resource "aws_kinesis_firehose_delivery_stream" "s3_delivery" {
  count       = var.firehose_endpoint_arn == null ? 1 : 0
  name        = var.stream_name
  destination = "extended_s3"

  extended_s3_configuration {
    role_arn   = aws_iam_role.firehose_s3.arn
    bucket_arn = var.firehose_bucket_arn == null ? aws_s3_bucket.firehose[0].arn : var.firehose_bucket_arn
  }
  tags = merge(local.tags, tomap({ "Name" : "kinesis_${var.stream_name}" }))
}

// Firehose assume role policy 
data "aws_iam_policy_document" "firehose_s3_assume_policy" {
  statement {
    effect = "Allow"

    principals {
      type        = "Service"
      identifiers = ["firehose.amazonaws.com"]
    }

    actions = ["sts:AssumeRole"]
  }
}

// Firehose access policy to S3 
data "aws_iam_policy_document" "firehose_s3_access_policy" {
  statement {
    actions = [
      "s3:ListBucket",
      "s3:GetBucketLocation",
      "s3:*Object"
    ]

    resources = [aws_s3_bucket.firehose[0].arn]

  }
}

// Firehose role to access S3
resource "aws_iam_role" "firehose_s3" {
  name               = "kinesis_s3_${var.stream_name}"
  assume_role_policy = data.aws_iam_policy_document.firehose_s3_assume_policy.json
  inline_policy {
    name   = "s3_access"
    policy = data.aws_iam_policy_document.firehose_s3_access_policy.json
  }
  tags = merge(local.tags, tomap({ "Name" : "kinesis_s3_${var.stream_name}" }))
}



