terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "5.3.0"
    }
  }
  /* backend "s3" {
    bucket = "bboxx-tf-state-login"
    key    = "DevOpsUserManagement"
    region = "eu-west-1"
  } */
}

// dev account
provider "aws" {
  region = "eu-west-1"
  /* assume_role {
    role_arn = "arn:aws:iam::068414940364:role/AccraDevOpsAdmin"
  } */
}



