// Global

variable "tags" {
  default     = {
    created_by = "c.etou"
  }
  description = "Please supply the Environment key at least. Name will be supplied here."
  type        = map(string)
}

variable "region" {
  description = "Deployment Region"
  type        = string
  default     = "eu-west-1"
}

variable "service_name" {
  description = "Application deployment name"
  type        = string
  default     = "KINESIS"
}

variable "environment" {
  description = "Deployment environment"
  type        = string
  default     = "dev"
}

variable "billing_group" {
  description = "department the service belong to"
  type        = string
  default     = "devops"
}


// Stream name
variable "stream_name" {
  description = "stream name"
  type        = string
  default     = "stream_tf"
}


// SNS Variables

variable "firehose_sns_topic_arn" {
  description = "SNS topic arn for existing topic"
  type        = string
  default     = null
}

variable "protocol" {
  type    = string
  default = "firehose"
}

variable "firehose_endpoint_arn" {
  type = string
  #  default = "arn:aws:firehose:us-east-1:068414940364:deliverystream/SNS-Kinesis-stream-test"
  default = null
}

variable "filter_policy_path" {
  type    = string
  default = null
}


// Firehose S3 bucket 
variable "firehose_bucket_arn" {
  description = "s3 bucket arn for existing bucket"
  type = string
  default = null
}