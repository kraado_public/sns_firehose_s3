
output "firehose" {
  value = {
    bucket_arn  = aws_s3_bucket.firehose[0].arn
    s3_role_arn = aws_iam_role.firehose_s3.arn
    sns_arn     = aws_sns_topic.firehose_sns[0].arn
    sns_role_arn = aws_iam_role.firehose_sns.arn
    stream_arn  = aws_kinesis_firehose_delivery_stream.s3_delivery[0].arn
  }

}
