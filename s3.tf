#-----------------------------------------
# CREATE S3 BUCKET
#-----------------------------------------
// Create bucket with var.firehose_bucket_arn = null
resource "aws_s3_bucket" "firehose" {
  count = var.firehose_bucket_arn == null ? 1 : 0
  bucket = local.s3_name

  tags = merge(local.tags, tomap({ "Name" : local.s3_name }))
}


