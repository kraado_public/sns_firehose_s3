#-----------------------------------------
# CREATE SNS TOPIC
#-----------------------------------------
// create an SNS topic if var.firehose_sns_topic_arn is null
resource "aws_sns_topic" "firehose_sns" {
  count        = var.firehose_sns_topic_arn == null ? 1 : 0
  name         = "firehose_sns_${var.stream_name}"
  display_name = "firehose_sns_${var.stream_name}"

  tags = merge(local.tags, tomap({ "Name" : "firehose_sns_${var.stream_name}" }))
}


#-----------------------------------------
# KINESIS SNS TOPIC SUBSCRIPTION
#-----------------------------------------
resource "aws_sns_topic_subscription" "firehose_sns" {
  topic_arn             = var.firehose_sns_topic_arn == null ? aws_sns_topic.firehose_sns[0].arn : var.firehose_sns_topic_arn
  protocol              = var.protocol
  endpoint              = local.firehose_endpoint
  subscription_role_arn = aws_iam_role.firehose_sns.arn
  #subscription_role_arn = time_sleep.role_propagation.triggers["sns_role"]
  filter_policy = var.filter_policy_path == null ? file("${path.module}/filter_policies/blank.json") : file("${var.filter_policy_path}")
  depends_on    = [aws_iam_role.firehose_sns, aws_sns_topic.firehose_sns[0]]
}


/* // Introducing 30s delay between 
resource "time_sleep" "role_propagation" {

  create_duration = "30s"
  triggers = {
    firehose = aws_sns_topic.firehose_sns[0].arn
    sns_role = aws_iam_role.firehose_sns.arn
  }
} */


#-----------------------------------------
# SNS IAM ROLE AND PERMISSION
#-----------------------------------------
// SNS to use this role to access Kinesis through subscription
resource "aws_iam_role" "firehose_sns" {
  name               = "firehose_sns_${var.stream_name}"
  assume_role_policy = data.aws_iam_policy_document.firehose_sns_assume_policy.json
  inline_policy {
    name   = "firehose_access"
    policy = data.aws_iam_policy_document.firehose_sns_access_policy.json
  }

  tags = merge(local.tags, tomap({ "Name" : "firehose_sns_${var.stream_name}" }))

}

// SNS assume_policy document
data "aws_iam_policy_document" "firehose_sns_assume_policy" {
  statement {
    effect  = "Allow"
    actions = ["sts:AssumeRole"]

    principals {
      type        = "Service"
      identifiers = ["sns.amazonaws.com"]
    }
  }
}

// SNS permission to Kinesis endpoint
data "aws_iam_policy_document" "firehose_sns_access_policy" {
  statement {
    actions = [
      "firehose:DescribeDeliveryStream",
      "firehose:ListDeliveryStreams",
      "firehose:ListTagsForDeliveryStream",
      "firehose:PutRecord",
      "firehose:PutRecordBatch"
    ]

    resources = [local.firehose_endpoint]

  }
}

// additional sns permission to be attached to kinesis role
data "aws_iam_policy" "additional_sns" {
  name = "AmazonSNSRole"
}

resource "aws_iam_policy_attachment" "additional_sns" {
  name       = "SNSRole_attachment"
  roles      = [aws_iam_role.firehose_sns.name]
  policy_arn = data.aws_iam_policy.additional_sns.arn
}
